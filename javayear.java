import java.time.Year;
public class javayear {
    public static void main(String[] args) {
        Year year1 = Year.of(2023);
        Year year1Day150 = year1.atDay(150);
        System.out.println("Year 2023, day 150: " + year1Day150);
        Year year2 = Year.of(2024);
        Year year2Month7 = year2.atMonth(7);
        System.out.println("Year 2024, month 7: " + year2Month7);
        Year year3 = Year.of(2025);
        Year year4 = Year.of(2026);
        int comparisonResult = year3.compareTo(year4);
        System.out.println("Comparison result: " + comparisonResult);
        Year year5 = Year.of(2027);
        int year5Value = year5.getValue();
        System.out.println("Year 2027 value: " + year5Value);
    }
}
